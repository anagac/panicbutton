//
//  TodayViewController.swift
//  date safe extension
//
//  Created by Andres Aguilar on 2/17/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit
import NotificationCenter
import MessageUI

class TodayViewController: UIViewController, UINavigationControllerDelegate, NCWidgetProviding, MFMessageComposeViewControllerDelegate {
    

    let sharedDefaults = UserDefaults.init(suiteName: "group.anagac.date-safe");
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view from its nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        if let option1 = sharedDefaults!.object(forKey: "option1")  {
            print(option1);
            let option1Button = view.viewWithTag(1) as! UIButton;
            option1Button.setTitle(option1 as? String, for: .normal);
            
        }
        if let option2 = sharedDefaults!.object(forKey: "option2") {
            print(option2);
        }
        if let option3 = sharedDefaults!.object(forKey: "option3") {
            print(option3);
        }
        if let option4 = sharedDefaults!.object(forKey: "option4") {
            print(option4);
        }
        

        completionHandler(NCUpdateResult.newData)
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        let telegramUrl = URL.init(string: "tg://msg?text=Prueba&to=4077498066")
        extensionContext?.open(telegramUrl!, completionHandler: nil );
        
        
        let recipients = ["4077498066"]
        let message = "Prueba de Date Safe";
        let messageController = MFMessageComposeViewController.init();
        messageController.delegate = self;
        messageController.body = message;
        messageController.recipients = recipients;
        
        //present(messageController, animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
            case .cancelled:
                print("canceled");
                break;
            case .failed:
                print("failed");
                break;
            case .sent:
                print("sent");
                break;
        }
    }
}
