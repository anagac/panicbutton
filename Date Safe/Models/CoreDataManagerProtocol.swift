//
//  CoreDataManagerProtocol.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/16/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit
import CoreData

public protocol CoreDataManagerProtocol {
    var managedObjectContext: NSManagedObjectContext { get }
    
    func saveContext()
    func insertRecord(entityName: String) -> NSManagedObject
}


