//
//  HelperManager.swift
//  Date Safe
//
//  Created by Andres Aguilar on 4/25/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit

class HelperManager: NSObject {
    
    //Singleton
    static let sharedManager: HelperManager = {
        let instance = HelperManager()
        // setup code
        return instance
    }()
    
    func sanitizeText(_ text: String) -> String? {
        let trimmedText = text.trimmingCharacters(in: .whitespacesAndNewlines)
        return (trimmedText.isEmpty) ? nil : trimmedText;
    }
}
