//
//  AlertViewManager.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/3/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit

class AlertViewManager: NSObject {
    
    //Singleton
    static let sharedManager: AlertViewManager = {
        let instance = AlertViewManager()
        // setup code
        return instance
    }()
    
    func showAlert(title: String, message: String, inViewController view: UIViewController) {
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "Ok", style: .default)
        alertController.addAction(okAction)
        
        view.present(alertController, animated: true)
    }
}
