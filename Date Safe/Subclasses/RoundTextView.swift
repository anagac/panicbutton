//
//  RoundTextField.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/29/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit

class RoundTextField: UITextView {

    override func awakeFromNib() {
        super.awakeFromNib()
        layoutIfNeeded()
        makeRound()
        
    }
   
    func makeRound() {
        // Drawing code
        layer.cornerRadius = 4
        layer.borderWidth = 1
        layer.borderColor = UIColor.darkGray.cgColor
    }


}
