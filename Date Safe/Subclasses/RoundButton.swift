//
//  RoundButton.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/29/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit


class RoundButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layoutIfNeeded()
        makeRound()
        
    }

    func makeRound() {
        layer.cornerRadius = self.frame.size.height/2
    }
    

}
