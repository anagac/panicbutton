//
//  DateDetailsViewController.swift
//  Date Safe
//
//  Created by Andres Aguilar on 4/25/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit

class DateDetailsViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var detailsTextView: UITextView?
    @IBOutlet weak var placeholderLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textViewDidChange(_ textView: UITextView) {
        print("%@", textView.text)
        if let trimmedText = HelperManager.sharedManager.sanitizeText(textView.text) {
            if(trimmedText.characters.count > 0) {
                placeholder(isHidden: true);
                return;
            }
        }
        placeholder(isHidden: false);
    }
    
    private func placeholder(isHidden: Bool) {
        placeholderLabel?.isHidden = isHidden;
    }
}
