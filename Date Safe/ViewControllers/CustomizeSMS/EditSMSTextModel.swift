//
//  EditSMSTextModel.swift
//  Date Safe
//
//  Created by Andres Aguilar on 4/4/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit

enum InvalidTextMessage: Error {
    case emptyText
}

class EditSMSTextModel: NSObject {
    
    static let emergencyInitialText = NSLocalizedString("defaultEmergencyText", comment: "")
    static let indisposedInitialText = NSLocalizedString("defaultIndisposedText", comment: "")
    static let callMeInitialText = NSLocalizedString("defaultCallMeText", comment: "")
    
    let coreDataManager:CoreDataManagerProtocol
    
    init(manager: CoreDataManagerProtocol) {
        coreDataManager = manager
        super.init()
        
        if(!initialDataIsSet()) {
            setInitialData()
        }
    }
    
    //MARK: - Initializer methods
    private func initialDataIsSet() -> Bool{
        let count = messagesCount()
        return count>0 ? true: false
    }
    
    private func setInitialData() {
        createRecord(category: MessageCategory.emergency, text: EditSMSTextModel.emergencyInitialText)
        createRecord(category: MessageCategory.indisposed, text: EditSMSTextModel.indisposedInitialText)
        createRecord(category: MessageCategory.call, text: EditSMSTextModel.callMeInitialText)
        coreDataManager.saveContext()
    }
    
    private func createRecord(category: MessageCategory, text: String ) {
        let newMessage = coreDataManager.insertRecord(entityName: Message.getEntityName()) as! Message
        newMessage.body = text
        newMessage.messageCategory = category
    }
    
    //MARK: - Public methods
    public func messagesCount() -> Int {
        do {
            return try coreDataManager.managedObjectContext.count(for: Message.getFetchRequest())
        }catch {
            return -1;
        }
    }
    
    public func textForCategory(_ category: MessageCategory) -> String {
        if let fetchedMessage = fetchMessageManagedObject(for: category) {
            return fetchedMessage.body!
        }
        return ""
    }
    
    public func setText(_ text: String, for category: MessageCategory) throws {
        guard let sanitizedText = HelperManager.sharedManager.sanitizeText(text) else {
            throw InvalidTextMessage.emptyText
        }
        saveText(sanitizedText, for: category)
    }

    //MARK: - Private methods
    private func fetchMessageManagedObject(for category: MessageCategory) -> Message? {
        do {
            let fetchRequest = Message.getFetchRequest()
            fetchRequest.predicate = NSPredicate(format: "category == %@", category.rawValue)
            let fetchedContacts = try coreDataManager.managedObjectContext.fetch(fetchRequest)
            return fetchedContacts[0]
        } catch {
            //Fetch failed
            return nil
        }
    }
    
    private func saveText(_ text: String,  for category: MessageCategory) {
        if let message = fetchMessageManagedObject(for: category) {
            message.body = text
            coreDataManager.saveContext()
        }
    }
    
}
