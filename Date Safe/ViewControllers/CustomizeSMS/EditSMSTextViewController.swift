//
//  EditSMSTextViewController.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/29/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit

class EditSMSTextViewController: UIViewController {

    @IBOutlet weak var emergencyTextField: RoundTextField!
    @IBOutlet weak var indisposedTextField: RoundTextField!
    @IBOutlet weak var callMeTextField: RoundTextField!
    
    var editSMSTextModel : EditSMSTextModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editSMSTextModel = EditSMSTextModel.init(manager: CoreDataManager.sharedManager)
        loadMessages()
        // Do any additional setup after loading the view.
    }
    
    func loadMessages() {
        emergencyTextField.text = editSMSTextModel.textForCategory(.emergency)
        indisposedTextField.text = editSMSTextModel.textForCategory(.indisposed)
        callMeTextField.text = editSMSTextModel.textForCategory(.call)
    }
    
    func saveTextMessages() -> Bool {
        do {
            try editSMSTextModel.setText(emergencyTextField.text, for: .emergency)
            try editSMSTextModel.setText(indisposedTextField.text, for: .indisposed)
            try editSMSTextModel.setText(callMeTextField.text, for: .call)
            return true
        }catch {
            AlertViewManager.sharedManager.showAlert(title: "", message: NSLocalizedString("emptyTextError", comment: ""), inViewController: self)
            return false
        }
    }
    
    
}
