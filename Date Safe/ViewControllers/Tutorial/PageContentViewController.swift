//
//  PageContentViewController.swift
//  Date Safe
//
//  Created by Andres Aguilar on 4/17/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit

class PageContentViewController: UIViewController {
    
    @IBOutlet weak var infoTextView: UITextView!
    @IBOutlet weak var infoImageView: UIImageView!
    
    var index: Int!;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
