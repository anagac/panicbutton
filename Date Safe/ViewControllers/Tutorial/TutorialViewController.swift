//
//  TutorialViewController.swift
//  Date Safe
//
//  Created by Andres Aguilar on 4/17/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBOutlet weak var doneButton: UIButton?
    
    var pageViewController : UIPageViewController?
    var titlesArray: Array<String> = []
    var imagesArray: Array<String> = []
    var numberOfPages = 7
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for index in 1...numberOfPages  {
            let localizedKey = "tutoScreen\(index)"
            titlesArray.append(NSLocalizedString(localizedKey, comment: "")) 
        }
        initializePagerView();
        setPagecontrolAppearance()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        doneButton?.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getViewController(at index: Int) -> UIViewController? {
        if(index<0 || index >= numberOfPages) {
            return nil
        }
        let aViewController = storyboard?.instantiateViewController(withIdentifier: "PageViewContent")
        if let pageContentVC = aViewController as? PageContentViewController {
            //Accessing view to kick off the initialization of the view
            _ = pageContentVC.view;
            pageContentVC.infoTextView.text = titlesArray[index]
            pageContentVC.index = index
            return pageContentVC
        }else {
            return nil
        }
        
    }
    
    private func initializePagerView() {
        pageViewController?.dataSource = self
        pageViewController?.delegate = self
        if let pageContentVC = getViewController(at: 0) {
            pageViewController?.setViewControllers([pageContentVC], direction:UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        }
        
    }
    
    private func setPagecontrolAppearance() {
        let pageControlAppearance = UIPageControl.appearance()
        pageControlAppearance.currentPageIndicatorTintColor = UIColor.black;
        pageControlAppearance.backgroundColor = UIColor.white;
        pageControlAppearance.pageIndicatorTintColor = UIColor.lightGray;
    }
    
    // MARK: - IBActions
    @IBAction func doneButtonTapped(_ sender: Any) {
        let userDefaults = UserDefaults.standard;
        userDefaults.set("done", forKey: "tutorialDone");
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setTabBarRoot()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        if(segue.identifier == "EmbeddedPageController") {
            pageViewController = segue.destination as? UIPageViewController
           
        }
    }
    
    // MARK: - UIPageViewControllerDataSource
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
     
        if let pageContentVC = viewController as? PageContentViewController {
            if var index = pageContentVC.index {
                index = index + 1;
                if(index == titlesArray.count) {
                    return nil
                }
                return getViewController(at: index)
            }else {
                return nil
            }
        }
        
        return nil
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    
        if let pageContentVC = viewController as? PageContentViewController {
            if var index = pageContentVC.index {
                if(index == 0) {
                    return nil
                }
                index = index - 1;
                return getViewController(at: index)
            }else {
                return nil
            }
        }
        
        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return titlesArray.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    // MARK: - UIPageViewControllerDelegate
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let pageContentVC = pendingViewControllers[0] as? PageContentViewController {
            if let index = pageContentVC.index {
                if(index+1 == titlesArray.count) {
                    doneButton?.isHidden = false
                }
            }
        }
    }

}
