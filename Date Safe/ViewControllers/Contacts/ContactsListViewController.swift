//
//  ContactsTutorialViewController.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/2/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit
import ContactsUI

class ContactsListViewController: UIViewController, CNContactPickerDelegate {
    
  
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var tagTextField: UITextField!
    
    var contactsDataSource: ContactListDataSource!
    var contactsModel: ContactsModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactsDataSource = ContactListDataSource.init(withTableView:tableView)
        tableView.dataSource = contactsDataSource
        tableView.delegate = contactsDataSource
        
        contactsModel = ContactsModel.init(manager: CoreDataManager.sharedManager)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addressBookTapped(_ sender: Any) {
        
        let contactPicker = CNContactPickerViewController()
        contactPicker.displayedPropertyKeys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        contactPicker.delegate = self
        present(contactPicker, animated: true, completion: nil)
        
    }

    @IBAction func addTapped(_ sender: Any) {
        
        let numberAdded = numberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let tagAdded = tagTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if(validate(number:numberAdded!) == false) {
            AlertViewManager.sharedManager.showAlert(title: NSLocalizedString("generalWarning", comment: ""), message: NSLocalizedString("invalidPhoneWarning", comment: ""), inViewController: self)
            return
        }
        
        if(validate(tag:tagAdded!) == false) {
            AlertViewManager.sharedManager.showAlert(title: NSLocalizedString("generalWarning", comment: ""), message: NSLocalizedString("enterNameWarning", comment: ""), inViewController: self)
            return
        }
        saveNumber(phone: numberAdded!, tag: tagAdded!)
        numberTextField.text? = ""
        tagTextField.text? = ""
    }
    
    private func validate(number: String) -> Bool {
        
        let type: NSTextCheckingResult.CheckingType = [.phoneNumber]
        let detector = try? NSDataDetector(types: type.rawValue)
        let matches = detector?.matches(in: number, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0,number.characters.count))
        return matches!.count > 0
    }
    
    private func validate(tag: String) -> Bool {
        return (tag.characters.count > 0) ? true : false
    }
    
    private func removeSpecialCharacters(number: String) -> String {
        let characterSet = CharacterSet.init(charactersIn: "()- ")
        return number.components(separatedBy: characterSet).joined(separator: "")
    }
    
    private func saveNumber(phone: String, tag: String) {
        do {
            try _ = contactsModel.add(phone: phone, tag: tag)
        }catch SaveContactError.limitReached {
            //Called with a little delay due to ContactPickerDelegate returning animation
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                AlertViewManager.sharedManager.showAlert(title: NSLocalizedString("generalSorry", comment: ""), message: NSLocalizedString("limitReachedError", comment: ""), inViewController: self)
            })
        }catch SaveContactError.repeatedPhone {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                AlertViewManager.sharedManager.showAlert(title: NSLocalizedString("generalError", comment: ""), message: NSLocalizedString("repeatedPhoneError", comment: ""), inViewController: self)
            })
        }catch {
            
        }
        
    }
    
    //ContactPickerDelegate
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        let phoneNumber = contactProperty.value as! CNPhoneNumber;
        let phoneString = removeSpecialCharacters(number: phoneNumber.stringValue)
        let name = contactProperty.contact.givenName + " " + contactProperty.contact.familyName
        
        saveNumber(phone: phoneString, tag: name)
        
    }
}
