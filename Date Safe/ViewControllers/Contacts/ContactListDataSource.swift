//
//  ContactListDataSource.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/2/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//
import Foundation
import UIKit
import CoreData

class ContactListDataSource: NSObject, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {

    var fetchedResultsController: NSFetchedResultsController<Contact>!
    weak var tableView: UITableView?
    var contactsModel: ContactsModel
    
    init(withTableView tableView: UITableView) {
        self.tableView = tableView
        contactsModel = ContactsModel.init(manager: CoreDataManager.sharedManager)
        super.init()
        
        initializeFetchedResultsController()
       
    }
    
    func initializeFetchedResultsController() {
        let coreDataManager = CoreDataManager.sharedManager;
        let request = Contact.getFetchRequest()
        let orderSort = NSSortDescriptor(key: "order", ascending: true)
        request.sortDescriptors = [orderSort]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: coreDataManager.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ContactCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
        if(cell == nil) {
            cell = UITableViewCell.init(style: .value1, reuseIdentifier: identifier)
        }
                
        let selectedContact = fetchedResultsController.object(at: indexPath) as Contact
        cell?.textLabel?.text = selectedContact.tag;
        cell?.detailTextLabel?.text = selectedContact.phone;
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete) {
            let selectedContact = fetchedResultsController.object(at: indexPath)
            contactsModel.delete(contact:selectedContact)
        }
    }
    

    //NSFetchedResultsControllerDelegate
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
            case .insert:
                tableView?.insertRows(at: [newIndexPath!], with: .fade)
            case .delete:
                tableView?.deleteRows(at: [indexPath!], with: .fade)
            default:
                break
        }
    }

    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.endUpdates()
    }
}
