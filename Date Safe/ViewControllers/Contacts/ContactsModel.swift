//
//  ContactsModel.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/2/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import UIKit

enum SaveContactError: Error {
    case limitReached
    case repeatedPhone
}

class ContactsModel: NSObject {
    
    static let CONTACTS_LIMIT = 5
    
    let coreDataManager:CoreDataManagerProtocol
    
    init(manager: CoreDataManagerProtocol) {
        coreDataManager = manager
    }
    
    func add(phone: String, tag: String) throws -> Contact {
        let count = contactsCount()
        if( count >= ContactsModel.CONTACTS_LIMIT) {
            throw SaveContactError.limitReached
        }
        
        if(phoneAlreadyExists(phone: phone)) {
            throw SaveContactError.repeatedPhone
        }
        
        return insertContact(withPhone: phone, tag:tag ,andOrderNumber:count+1)
    }
    
    func delete(contact: Contact) {
        deleteFromDisk(contact:contact)
    }
    
    func getContacts() -> Array<Contact>{
        return getAllContacts()
    }
    
    //MARK: - Validators
    func contactsCount() -> Int {
        
        return getContactsCount()
    }
    
    func phoneAlreadyExists(phone: String) -> Bool {
        let contacts = getAllContacts()
        for contact: Contact in contacts {
            if(contact.phone == phone) {
                return true
            }
        }
        return false
    }
    
    //MARK: - Core Data Operations
    private func insertContact(withPhone phone: String, tag: String, andOrderNumber order: Int) -> Contact {
        let newContact = coreDataManager.insertRecord(entityName: Contact.getEntityName()) as! Contact
        newContact.phone = phone
        newContact.tag = tag
        newContact.order = Int32(order)
        coreDataManager.saveContext()
        return newContact
    }
    
    private func getAllContacts() -> Array<Contact> {
        
        do {
            let fetchedContacts = try coreDataManager.managedObjectContext.fetch(Contact.getFetchRequest())
            return fetchedContacts
        } catch {
            //Fetch failed
            return []
        }
        
    }
    
    private func deleteFromDisk(contact: Contact) {
        
        coreDataManager.managedObjectContext.delete(contact)
        coreDataManager.saveContext()
    }
    
    private func getContactsCount() -> Int {
        do {
            return try coreDataManager.managedObjectContext.count(for: Contact.getFetchRequest())
        }catch {
            return -1;
        }
    }
    
}
