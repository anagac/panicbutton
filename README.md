# Date Safe #

This app is meant to help people when going out with people they met in dating apps or other social media. You need to be extra careful in this kind of dates, because you may not know the other person well. 

The idea is to have a panic button easily accessible, by providing a Widget that you can set up in the Today View. Today View can be accessed by swiping right in the lock screen, or swiping down in any other place. 

This is a work still in progress.