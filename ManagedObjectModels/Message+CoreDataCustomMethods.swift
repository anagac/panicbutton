//
//  Contact+CoreDataCustomMethods.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/29/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import Foundation
import CoreData

enum MessageCategory: String {
    case emergency, indisposed, call
}

extension Message {
    
    var messageCategory : MessageCategory {
        get {
            return MessageCategory(rawValue:category!)!
        }
        set {
            category = newValue.rawValue
        }
    }
    
    @nonobjc public class func getEntityName() -> String {
        return "Message"
    }
    
    @nonobjc public class func getFetchRequest() -> NSFetchRequest<Message> {
        return NSFetchRequest<Message>(entityName:getEntityName());
    }
    
}
