//
//  Contact+CoreDataProperties.swift
//  
//
//  Created by Andres Aguilar on 3/23/17.
//
//

import Foundation
import CoreData


extension Contact {

    @NSManaged public var phone: String?
    @NSManaged public var tag: String?
    @NSManaged public var order: Int32

}
