//
//  Contact+CoreDataCustomProperties.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/23/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import Foundation
import CoreData

extension Contact {
    
    @nonobjc public class func getEntityName() -> String {
        return "Contact"
    }
    
    @nonobjc public class func getFetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName:getEntityName());
    }
}
