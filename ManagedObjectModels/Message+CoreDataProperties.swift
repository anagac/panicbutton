//
//  Message+CoreDataProperties.swift
//  
//
//  Created by Andres Aguilar on 3/29/17.
//
//

import Foundation
import CoreData


extension Message {

    @NSManaged public var category: String?
    @NSManaged public var body: String?

}
