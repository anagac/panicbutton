//
//  CoreDataTestCase.swift
//  Date Safe
//
//  Created by Andres Aguilar on 3/17/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import XCTest
import CoreData
@testable import Date_Safe

class FakeCoreDataManager: NSObject, CoreDataManagerProtocol {
    
    var storeCoordinator: NSPersistentStoreCoordinator
    var managedObjectContext: NSManagedObjectContext
    
    var managedObjectModel: NSManagedObjectModel
    var store: NSPersistentStore
    
    override init() {
        managedObjectModel = NSManagedObjectModel.mergedModel(from: nil)!
        storeCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        do {
            try store = storeCoordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
        }catch {
            print("Failed to initialize core data")
            abort()
        }
        
        
        managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = storeCoordinator
    }
    
    func cleanUp() -> Bool{
        
        do {
            try storeCoordinator.remove(store)
            return true
        }catch {
            return false
        }
    }
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    func insertRecord(entityName: String) -> NSManagedObject {
        let newRecord = NSEntityDescription.insertNewObject(forEntityName: entityName, into: managedObjectContext)
        return newRecord
    }
    
}

class CoreDataTestCase: XCTestCase {
    
    var fakeCoreDataManager: FakeCoreDataManager!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        fakeCoreDataManager = FakeCoreDataManager()
        
    }

    
}
