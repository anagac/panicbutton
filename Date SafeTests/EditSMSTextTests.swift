//
//  EditSMSTextTests.swift
//  Date Safe
//
//  Created by Andres Aguilar on 4/4/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import XCTest
@testable import Date_Safe

class EditSMSTextTests: CoreDataTestCase {
    
    var editSMSTextModel: EditSMSTextModel!
    
    override func setUp() {
        super.setUp()
        //Fake Core Data Manager initialized in parent
        editSMSTextModel = EditSMSTextModel.init(manager: fakeCoreDataManager)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        XCTAssert(fakeCoreDataManager.cleanUp(), "Failed to remove store coordinator")
        super.tearDown()
        
    }
    
    func testInitialSetting() {
        let messagesCount = editSMSTextModel.messagesCount()
        XCTAssertEqual(messagesCount, 3, "Count is not 3")
    }
    
    func testInitialEmergencyText() {
        let emergencyText = editSMSTextModel.textForCategory(.emergency)
        XCTAssertEqual(emergencyText, EditSMSTextModel.emergencyInitialText, "Emergency text not equal")
    }
    
    func testInitialIndisposedText() {
        let indisposedText = editSMSTextModel.textForCategory(.indisposed)
        XCTAssertEqual(indisposedText, EditSMSTextModel.indisposedInitialText, "Indisposed text not equal")
    }
    
    func testInitialCallMeText() {
        let callMe = editSMSTextModel.textForCategory(.call)
        XCTAssertEqual(callMe, EditSMSTextModel.callMeInitialText, "Call Me text not equal")
    }
    
    func testSettingEmergencyText() {
        let newText = "I'm in trouble, this is not a joke, please call 911."
        do {
            try editSMSTextModel.setText(newText, for: .emergency)
        }catch {
            XCTFail("Empty text")
        }
        
        let savedText = editSMSTextModel.textForCategory(.emergency)
        XCTAssertEqual(newText, savedText, "Set Emergency text failed")
        
    }
    
    func testSettingIndisposedText() {
        let newText = "I had a little bit too much. Come pick me up."
        do {
            try editSMSTextModel.setText(newText, for: .indisposed)
        } catch {
            XCTFail("Empty text")
        }
        
        let savedText = editSMSTextModel.textForCategory(.indisposed)
        XCTAssertEqual(newText, savedText, "Set Indisposed text failed")
    }
    
    func testSettingCallMeText() {
        let newText = "This date is not going well. Call me!"
        do {
            try editSMSTextModel.setText(newText, for: .call)
        } catch {
            XCTFail("Empty text")
        }
        
        let savedText = editSMSTextModel.textForCategory(.call)
        XCTAssertEqual(newText, savedText, "Set Call Me text failed")
    }
    
    func testTrimmingText() {
        var newText = "        This date is not going well. Call me!        "
        do {
            try editSMSTextModel.setText(newText, for: .call)
        } catch {
            XCTFail("Empty text")
        }
        
        newText = newText.trimmingCharacters(in: .whitespacesAndNewlines)
        let savedText = editSMSTextModel.textForCategory(.call)
        XCTAssertEqual(newText, savedText, "Set Call Me text failed")
    }
    
    func testSettingEmptyString() {
        let newText = ""
        do {
            try editSMSTextModel.setText(newText, for: .call)
        } catch let e as InvalidTextMessage {
            XCTAssertEqual(e, InvalidTextMessage.emptyText)
        } catch {
            XCTFail("Failed setting")
        }
    }

    
}
