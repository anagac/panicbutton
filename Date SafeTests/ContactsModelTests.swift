//
//  Date_SafeTests.swift
//  Date SafeTests
//
//  Created by Andres Aguilar on 2/17/17.
//  Copyright © 2017 Andres Aguilar. All rights reserved.
//

import XCTest
@testable import Date_Safe

class ContactsModelTests: CoreDataTestCase {
    
    var contactsModel : ContactsModel!
    
    override func setUp() {
        super.setUp()
        //Fake Core Data Manager initialized in parent
        contactsModel = ContactsModel.init(manager: fakeCoreDataManager)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        XCTAssert(fakeCoreDataManager.cleanUp(), "Failed to remove store coordinator")
        super.tearDown()
        
    }
    
    func contactsCount(shouldBe shouldBeCount: Int) {
        do {
            let contactsCount = try fakeCoreDataManager.managedObjectContext.count(for: Contact.getFetchRequest())
            XCTAssertEqual(contactsCount, shouldBeCount)
        } catch {
            XCTFail("Failed counting")
        }
    }
    
    func testAdd() {
        do {
            try _ = contactsModel.add(phone: "1261252512", tag: "Andres")
        }catch {
            XCTFail("Failed inserting")
        }
        
        contactsCount(shouldBe: 1)
    }
    
    func testRemove() {
        do {
            let contact = try contactsModel.add(phone: "1261252512", tag: "Andres")
            contactsModel.delete(contact: contact)
        }catch {
            XCTFail("Failed inserting")
            return
        }
        
        contactsCount(shouldBe: 0)

    }
    
    func testGetContacts() {
        do {
            _ = try contactsModel.add(phone: "1261252512", tag: "Andres")
            _ = try contactsModel.add(phone: "1616125423", tag: "Rene")
            _ = try contactsModel.add(phone: "6197525213", tag: "Tere")
            
        }catch {
            XCTFail("Failed inserting")
            return
        }
        
        contactsCount(shouldBe: 3)
    }
    
    func testRepeatedNumber() {
        do {
            _ = try contactsModel.add(phone: "1261252512", tag: "Andres")
            _ = try contactsModel.add(phone: "1261252512", tag: "Rene")
        } catch let e as SaveContactError {
            XCTAssertEqual(e, SaveContactError.repeatedPhone)
        } catch {
            XCTFail("Failed inserting")
        }
        contactsCount(shouldBe: 1)
    }
    
    func testLimitReached() {
        do {
            _ = try contactsModel.add(phone: "1261252512", tag: "Andres")
            _ = try contactsModel.add(phone: "1616125423", tag: "Rene")
            _ = try contactsModel.add(phone: "6197525213", tag: "Tere")
            _ = try contactsModel.add(phone: "7519807234", tag: "Rob")
            _ = try contactsModel.add(phone: "1527982475", tag: "Susy")
            _ = try contactsModel.add(phone: "5129307125", tag: "Susy")
        } catch let e as SaveContactError {
            XCTAssertEqual(e, SaveContactError.limitReached)
            return
        } catch {
            XCTFail("Failed inserting")
        }
        contactsCount(shouldBe: ContactsModel.CONTACTS_LIMIT)

    }
    
}
